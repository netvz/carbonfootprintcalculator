package com.example.demo4;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HelloController implements Initializable {
    @FXML
    protected TextField electricBillField;
    @FXML
    protected TextField gasBillField;
    @FXML
    protected TextField oilBillField;
    @FXML
    protected TextField carMileageField;
    @FXML
    protected TextField flightsOneField;
    @FXML
    protected TextField flightsTwoField;
    @FXML
    protected CheckBox check1;
    @FXML
    protected CheckBox check2;
    @FXML
    protected CheckBox check3;
    @FXML
    protected CheckBox check4;
    @FXML
    protected ChoiceBox<String> chooseCountry;
    @FXML
    protected Label countryCarbon;
    @FXML
    protected Label yourCarbon;
    @FXML
    protected Label worldCarbon;

    protected double electricBill;
    protected double gasBill;
    protected double oilBill;
    protected double carMileage;
    protected double flightsOne;
    protected double flightsTwo;
    protected double paper;
    protected double alu;
    protected double sum;

    public void calculate(){
        electricBill = Double.parseDouble(electricBillField.getText());
        gasBill = Double.parseDouble(gasBillField.getText());
        oilBill = Double.parseDouble(oilBillField.getText());
        carMileage = Double.parseDouble(carMileageField.getText());
        flightsOne = Double.parseDouble(flightsOneField.getText());
        flightsTwo = Double.parseDouble(flightsTwoField.getText());

        if(check1.isSelected()){
            paper = 0;
        }else if(check2.isSelected()){
            paper = 184;
        }

        if(check3.isSelected()){
            alu = 0;
        }else if(check4.isSelected()){
            alu = 166;
        }

        sum = electricBill+gasBill+oilBill+carMileage+(flightsOne*1100)+(flightsTwo*4400)+paper+alu;
        System.out.println(sum);

        String countryName = chooseCountry.getValue();
        Statement statement;
        try {
            statement = DatabaseConnection.getConnection().createStatement();
            assert statement != null;
            ResultSet valueToCompare = statement.executeQuery("SELECT country, carbonPC FROM carbon");
            while(valueToCompare.next()) {
                String countryChoosen = valueToCompare.getString("country");
                System.out.println(countryChoosen);
                if(countryName.equals(countryChoosen)) {
                    countryCarbon.setText((valueToCompare.getString("carbonPC")));
                    System.out.println(valueToCompare.getString("carbonPC"));
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        yourCarbon.setText(String.valueOf(sum));
        worldCarbon.setText("4400");

    }


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        Statement statement = null;
        try {
            statement = DatabaseConnection.getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            assert statement != null;
            chooseCountry.getItems().addAll(countriesToList(statement.executeQuery("SELECT country FROM carbon")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static String[] countriesToList(ResultSet resultSet) throws SQLException {
        ArrayList<String> countries = new ArrayList<>();
        while(resultSet.next()) {
            String countryName = resultSet.getString("country");
            countries.add(countryName);
            System.out.println("dupa");
        }
        String[] countryRoadsTakeMeHome = new String[countries.size()];
        return countries.toArray(countryRoadsTakeMeHome);
    }

}